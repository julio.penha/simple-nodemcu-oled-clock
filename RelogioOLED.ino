#include "SSD1306Wire.h"
#include <ESP8266WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include <iostream>

WiFiUDP ntpUDP;
SSD1306Wire display(0x3c, D1, D2);

const long utcOffsetInSeconds = -10800;

NTPClient timeClient(ntpUDP, "b.ntp.br", utcOffsetInSeconds);

char daysOfTheWeek[7][5] = {"dom", "seg", "ter", "qua", "qui", "sex", "sáb"};
char monthOfTheYear[12][4] = {"jan", "fev", "mar", "abr", "mai", "jun",
                              "jul", "ago", "set", "out", "nov", "dez"};

void startWiFi() {
  WiFi.begin("YOUR_SSID", "YOUR_PASSWORD");
  Serial.println("Connecting");

  while (WiFi.status() != WL_CONNECTED) {
    delay(250);
    Serial.print('.');
  }

  Serial.print("Conectado a rede: ");
  Serial.println(WiFi.SSID());
  Serial.print("Endereco IP: ");
  Serial.println(WiFi.localIP());
}

inline String get2digits(uint32_t n) {
  short len = 2;
  std::string result(len--, '0');
  for (int val = (n < 0) ? -n : n; len >= 0 && val != 0; --len, val /= 10)
    result[len] = '0' + val % 10;
  if (len >= 0 && n < 0)
    result[0] = '-';
  return result.c_str();
}

void setup() {
  Serial.begin(115200);
  delay(10);

  startWiFi();
  timeClient.begin();

  display.init();
  display.flipScreenVertically();
}

void loop() {
  timeClient.update();

  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);

  display.setFont(ArialMT_Plain_10);
  display.drawString(63, 10,
                     String(daysOfTheWeek[timeClient.getDay()]) + ", " +
                         String(timeClient.getDate()) + " de " +
                         String(monthOfTheYear[timeClient.getMonth() - 1]));

  display.setFont(ArialMT_Plain_24);
  display.drawString(29, 29, get2digits(timeClient.getHours()));
  display.drawString(45, 28, ":");
  display.drawString(62, 29, get2digits(timeClient.getMinutes()));
  display.drawString(78, 28, ":");
  display.drawString(95, 29, get2digits(timeClient.getSeconds()));

  display.display();

  delay(1000);
